#
# File specifying the location of Pythia 8 to use.
#

set( PYTHIA8_LCGVERSION 240 )
set( PYTHIA8_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/pythia8/${PYTHIA8_LCGVERSION}/${LCG_PLATFORM} )
